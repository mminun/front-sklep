package mypkg;



import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EchoServlet extends HttpServlet {

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {

        response.setContentType("text/html; charset=UTF-8");

        PrintWriter out = response.getWriter();

        //odpowiedz
        try {
            out.println("<!DOCTYPE html>");
            out.println("<html><head>");
            out.println("<meta http-equiv='Content-Type' content='text/html; charset=UTF-8'>");
            out.println("<title>Echo Servlet</title></head>");
            out.println("<body><h2>PODANE WARTOŚCI:</h2>");


            String username = request.getParameter("username");

            if (username == null
                    || (username = htmlFilter(username.trim())).length() == 0) {
                out.println("<p>Name: BRAK</p>");
            } else {
                out.println("<p>Name: " + username + "</p>");
            }


            String password = request.getParameter("password");
            if (password == null
                    || (password = htmlFilter(password.trim())).length() == 0) {
                out.println("<p>nazwisko: brak</p>");
            } else {
                out.println("<p>nazwisko: " + password + "</p>");
            }


            String gender = request.getParameter("gender");

            if (gender == null) {
                out.println("<p>email: brak</p>");
            } else {
                out.println("<p>email:" + gender + "</p>");
            }


            String[] languages = request.getParameterValues("language");

            if (languages == null || languages.length == 0) {
                out.println("<p>Languages: NONE</p>");
            } else {
                out.println("<p>Aktywny: ");
                for (String language : languages) {
                    if (language.equals("c")) {
                        out.println("tak");
                    } else if (language.equals("cs")) {
                        out.println("nie");
                    }
                }
                out.println("</p>");
            }


            // nazwy request param
            out.println("<a href='form_input.html'>powrót</a>");

            out.println("</body></html>");
        } finally {
            out.close();  //zamyka output writer
        }
    }

    //zamienia post na get
    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        doGet(request, response);
    }

    // Filter the string for special HTML characters to prevent
    // command injection attack
    private static String htmlFilter(String message) {
        if (message == null) return null;
        int len = message.length();
        StringBuffer result = new StringBuffer(len + 20);
        char aChar;

        for (int i = 0; i < len; ++i) {
            aChar = message.charAt(i);
            switch (aChar) {
                case '<': result.append("&lt;"); break;
                case '>': result.append("&gt;"); break;
                case '&': result.append("&amp;"); break;
                case '"': result.append("&quot;"); break;
                default: result.append(aChar);
            }
        }
        return (result.toString());
    }
}