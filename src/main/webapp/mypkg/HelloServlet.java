// To save as "<CATALINA_HOME>\webapps\helloservlet\WEB-INF\src\mypkg\HelloServlet.java"
package mypkg;

import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;

public class HelloServlet extends HttpServlet {
    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {

        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();


        try {
            out.println("<!DOCTYPE html>");
            out.println("<html><head><link rel=\"stylesheet\" type=\"text/css\" " +
                    "href=\"http://localhost:8081/webapp/resources/style.css\">");
            out.println("<meta http-equiv='Content-Type' content='text/html; charset=UTF-8'>");
            out.println("<title>Birdy</title></head>");
            out.println("<body>" +
                    "<div style=\"background-color:lightblue\">");
            out.println("<h1>SUPER FRĄTĘTdd</h1>" +
                    "</div>");
            out.println("<p>Request URI: " + request.getRequestURI() + "</p>");
            out.println("<p>Protocol: " + request.getProtocol() + "</p>");
            out.println("<p>PathInfo: " + request.getPathInfo() + "</p>");
            out.println("<p>Remote Address: " + request.getRemoteAddr() + "</p>");
            out.println("<p>Losowy numer: <strong>" + Math.random() + "</strong></p>");
            out.println("<a href='form_input.html'>FORMULARZ</a>" +
                    "<body background=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAASwAAACoCAMAAABt9SM9AAAAA1BMVEX/wMversVBAAAAR0lEQVR4nO3BAQEAAACCIP+vbkhAAQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAO8GxYgAAb0jQ/cAAAAASUVORK5CYII=\">");

            out.println("</body>");
            out.println("</html>");
        } finally {
            out.close();
        }
    }
}